function alphabetize(a) {
  return a.toLowerCase().split("").sort().join("").trim();
}

function getAnagramsOf(typedText) {
  let typedTextAlfabetico = alphabetize(typedText)
  let anagramas = []
  
  for (let i = 0; i < palavras.length; i++){   
    
    if (typedText.length === palavras[i].length) {
      let palavrasAlfabetico = alphabetize(palavras[i])
      
      if (typedTextAlfabetico === palavrasAlfabetico){
        anagramas.push(palavras[i])
      }
    }
  }
  return anagramas
}

const button = document.getElementById("findButton");
let divResultado = document.createElement('div')

button.addEventListener("click", function () {
  let typedText = document.getElementById("input").value;  

  divResultado.innerText = ''
  divResultado.innerText = getAnagramsOf(typedText)
  document.body.appendChild(divResultado)
});
